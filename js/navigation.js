$(document).ready(function () {

    var navigation = {
        elements: {
            main_nav: $("#navigation>nav>ul>li>a")
        },
        main: function () {
            navigation.elements.main_nav.click(navigation.go);
        },
        go: function () {
            if (location.pathname.replace(/^\//, '') === this.pathname.replace(/^\//, '') && location.hostname === this.hostname) {
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                if (target.length) {
                    $('html, body').animate({
                        scrollTop: target.offset().top - 51
                    }, 1000);
                    return false;
                }
            }
        }
    };
    
    navigation.main();
    
    var scroll = {
        elements: {
            fixed_nav: $("#navigation")
        },
        main: function () {
            scroll.toggle();
            $(window).on("scroll", scroll.toggle);
        },
        toggle: function () {
            var scrollPos = $(window).scrollTop();
            if (scrollPos < window.innerHeight - 51) {
                scroll.elements.fixed_nav.css("background", "rgba(0,0,0,0.5)");
            } else {
                scroll.elements.fixed_nav.css("background", "rgba(0,0,0,1)");
            }
        }
    };

    scroll.main();
    
});
